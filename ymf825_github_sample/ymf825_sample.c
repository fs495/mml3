/*
 * YMF825 sample
 *
 * Converted for Raspberry pi from here:
 * https://github.com/yamaha-webmusic/ymf825board/blob/master/sample1/ymf825board_sample1/ymf825board_sample1.ino
 *
 * MIT License
 * 
 * Copyright (c) 2017 Yamaha Corporation

 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#define OUTPUT_power 1

static int fd;
static struct spi_ioc_transfer tr[2];

void delay(unsigned msec)
{
    usleep(msec * 1000);
}

void if_write(char addr,unsigned char* data,char num){
    char i;
    char snd;

    tr[0].tx_buf = (uintptr_t)&addr;
    tr[0].len = 1;
    tr[1].tx_buf = (uintptr_t)data;
    tr[1].rx_buf = 0;
    tr[1].len = num;
    ioctl(fd, SPI_IOC_MESSAGE(2), tr);
}

void if_s_write(char addr,unsigned char data){
    if_write(addr,&data,1);
}

unsigned char if_s_read(char addr){
    unsigned char rcv;
    addr |= 0x80;
    tr[0].tx_buf = (uintptr_t)&addr;
    tr[0].len = 1;
    tr[1].tx_buf = 0;
    tr[1].rx_buf = (uintptr_t)&rcv;
    tr[1].len = 1;
    ioctl(fd, SPI_IOC_MESSAGE(2), tr);
    return rcv;
}

void init_825(void) {
    if_s_write( 0x1D, OUTPUT_power );
    if_s_write( 0x02, 0x0E );
    delay(1);
    if_s_write( 0x00, 0x01 );//CLKEN
    if_s_write( 0x01, 0x00 ); //AKRST
    if_s_write( 0x1A, 0xA3 );
    delay(1);
    if_s_write( 0x1A, 0x00 );
    delay(30);
    if_s_write( 0x02, 0x04 );//AP1,AP3
    delay(1);
    if_s_write( 0x02, 0x00 );
    //add
    if_s_write( 0x19, 0xF0 );//MASTER VOL
    if_s_write( 0x1B, 0x3F );//interpolation
    if_s_write( 0x14, 0x00 );//interpolation
    if_s_write( 0x03, 0x01 );//Analog Gain
   
    if_s_write( 0x08, 0xF6 );
    delay(21);
    if_s_write( 0x08, 0x00 );
    if_s_write( 0x09, 0xF8 );
    if_s_write( 0x0A, 0x00 );
   
    if_s_write( 0x17, 0x40 );//MS_S
    if_s_write( 0x18, 0x00 );
}

void set_tone(void){
    unsigned char tone_data[35] ={
	0x81,//header
	//T_ADR 0
	0x01,0x85,
	0x00,0x7F,0xF4,0xBB,0x00,0x10,0x40,
	0x00,0xAF,0xA0,0x0E,0x03,0x10,0x40,
	0x00,0x2F,0xF3,0x9B,0x00,0x20,0x41,
	0x00,0xAF,0xA0,0x0E,0x01,0x10,0x40,
	0x80,0x03,0x81,0x80,
    };
  
    if_s_write( 0x08, 0xF6 );
    delay(1);
    if_s_write( 0x08, 0x00 );
  
    if_write( 0x07, &tone_data[0], 35 );//write to FIFO
}

void set_ch(void){
    if_s_write( 0x0F, 0x30 );// keyon = 0
    if_s_write( 0x10, 0x71 );// chvol
    if_s_write( 0x11, 0x00 );// XVB
    if_s_write( 0x12, 0x08 );// FRAC
    if_s_write( 0x13, 0x00 );// FRAC  
}

void keyon(unsigned char fnumh, unsigned char fnuml){
    if_s_write( 0x0B, 0x00 );//voice num
    if_s_write( 0x0C, 0x54 );//vovol
    if_s_write( 0x0D, fnumh );//fnum
    if_s_write( 0x0E, fnuml );//fnum
    if_s_write( 0x0F, 0x40 );//keyon = 1  
}

void keyoff(void){
    if_s_write( 0x0F, 0x00 );//keyon = 0
}

void setup() {
    fd = open("/dev/spidev0.0", O_RDWR);
    if(fd < 0) {
	fprintf(stderr, "Error: sd1_open: cannot open\n");
	exit(1);
    }

    uint8_t bits = 8;
    int ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if(ret < 0) {
	fprintf(stderr, "Error: ioctl(SPI_IOC_WR_BITS_PER_WORD, %d)\n", bits);
	exit(1);
    }

    unsigned speed = 1 * 1000 * 1000;
    ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if(ret < 0) {
	fprintf(stderr, "Error: ioctl(SPI_IOC_WR_MAX_SPEED_HZ, %d)\n",
		speed);
	exit(1);
    }

    unsigned mode = 0;
    ret = ioctl(fd, SPI_IOC_WR_MODE, &mode);
    if(ret < 0) {
	fprintf(stderr, "Error: ioctl(SPI_IOC_WR_MODE, %d)\n", mode);
	exit(1);
    }

    init_825();
    set_tone();
    set_ch();
}

void loop() {
    keyon(0x14,0x65);
    delay(500);
    keyoff();
    delay(200);
    keyon(0x1c,0x11);
    delay(500);
    keyoff();
    delay(200);
    keyon(0x1c,0x42);
    delay(500);
    keyoff();
    delay(200);
    keyon(0x1c,0x5d);
    delay(500);
    keyoff();
    delay(200);
    keyon(0x24,0x17);
    delay(500);
    keyoff();
    delay(200);
}

int main(int argc, char *argv[])
{
    setup();
    for(;;)
	loop();
    return 0;
}
