#include <stdint.h>
#include <stdio.h>
#include "varray.h"
#include "common.h"

struct varray *null_mmle;
struct varray *tones;
struct varray *macros;
struct varray *tracks;

/* MML要素 */

const char *note_to_str[] = {
    "C", "C#", "D", "D#", "E",
    "F", "F#", "G", "G#", "A", "A#", "B",
};

void print_mmle_list(struct varray *a)
{
    for(int i = 0; i < a->used; i++) {
	uintptr_t mmle = varray(a, i);
	uint8_t type = mmle_type(mmle);
	if(type < 12) {
	    printf("\tnote %s, flag 0x%x, len %d\n",
		   note_to_str[type], mmle_flag(mmle), mmle_val(mmle));
	} else if(type < 128) {
	    printf("\ttype '%c', flag 0x%x, len %d\n",
		   type, mmle_flag(mmle), mmle_val(mmle));
	} else {
	    printf("\ttype %d, flag 0x%x, len %d\n",
		   type, mmle_flag(mmle), mmle_val(mmle));
	}
    }
}
