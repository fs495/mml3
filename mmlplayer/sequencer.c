#include <time.h>
#include <stdio.h>
#include "varray.h"
#include "common.h"

const unsigned long long nsec = 1000ull * 1000 * 1000;

/*----------------------------------------------------------------------
 */
/*
 * トラックごとの演奏コンテクスト
 */
static unsigned nr_tr;
static struct varray *tr_c_list;

/*----------------------------------------------------------------------
 */

static void seq_note_on(struct track_ctx *cp)
{
    printf(" note_on:  tr %u note %u step %u+%utb\n",
	   cp->track_nr, cp->note, cp->on_tb, cp->off_tb);
    drv->note_on(cp);
}

static void seq_note_off(struct track_ctx *cp)
{
    printf(" note_off: tr %d note %d\n", cp->track_nr, cp->note);
    drv->note_off(cp);
}

static void seq_change_tone(struct track_ctx *cp, unsigned tone)
{
    printf(" change_tone: tr %d tone %d\n", cp->track_nr, tone);
    cp->tone_nr = tone;
}

static void seq_next_note(struct track_ctx *cp)
{
    cp->curr++;
    printf(" seq_next_note: tr %d, %d/%d\n", cp->track_nr, cp->curr, cp->track->used);
    if(cp->curr >= cp->track->used)
	return;

    uintptr_t mmle = varray(cp->track, cp->curr);
    uint8_t type = mmle_type(mmle);

    if(type < 12) {
	/* ノートオン。番号はMIDI note on/offと同じ */
	cp->note = cp->octave * 12 + type + 24;
	unsigned tb = mmle_val(mmle);
	tb = (tb == LEN_DEFAULT) ? cp->default_tb
	    : (tb == LEN_DEFAULT32) ? cp->default_tb * 3 / 2 : tb;
	cp->on_tb = (mmle_flag(mmle) & FLAG_FULL_GATE) ? tb
	    : tb * cp->gate_ratio / 128;
	cp->off_tb = tb - cp->on_tb;
	seq_note_on(cp);

    } else if(type == 'r') {
	/* 休符 */
	unsigned tb = mmle_val(mmle);
	tb = (tb == LEN_DEFAULT) ? cp->default_tb
	    : (tb == LEN_DEFAULT32) ? cp->default_tb * 3 / 2 : tb;
	cp->on_tb = 0;
	cp->off_tb = tb;
	cp->note = -1;

    } else if(type == 't') {
	cp->on_tb = cp->off_tb = 0;
	cp->nsec_per_tb = 240 * nsec / WN_TB / mmle_val(mmle);

    } else if(type == 'l') {
	cp->on_tb = cp->off_tb = 0;
	cp->default_tb = WN_TB / mmle_val(mmle);

    } else if(type == 'q') {
	cp->on_tb = cp->off_tb = 0;
	cp->gate_ratio = mmle_val(mmle);

    } else if(type == 'o') {
	cp->on_tb = cp->off_tb = 0;
	cp->octave = mmle_val(mmle);

    } else if(type == '<') {
	cp->on_tb = cp->off_tb = 0;
	cp->octave++;

    } else if(type == '>') {
	cp->on_tb = cp->off_tb = 0;
	cp->octave--;

    } else if(type == '@') {
	cp->on_tb = cp->off_tb = 0;
	seq_change_tone(cp, mmle_val(mmle));
    }
}

void seq_init(void)
{
    nr_tr = (tracks->used >= max_track ? max_track : tracks->used);
    tr_c_list = varray_new();

    /* 演奏コンテクストを初期化してtr_c_listに追加 */
    for(int tr = 0; tr < nr_tr; tr++) {
	struct track_ctx *cp = malloc(sizeof(struct track_ctx));
	cp->track = (struct varray *)varray(tracks, tr);
	cp->track_nr = tr;
	cp->curr = -1;

	cp->on_tb = cp->off_tb = 0;
	cp->ellapse = 0;

	cp->octave = 4;
	cp->tone_nr = 0;
	cp->volume = 31;

	cp->nsec_per_tb = 
	cp->default_tb = WN_TB / 4; /* 四分音符(l4)に相当 */
	cp->gate_ratio = 128;

	cp->stack_dep = 0;

	seq_next_note(cp);
	varray_append(tr_c_list, (uintptr_t)cp);
    }
}

static void seq_play_delta(unsigned nsec_from_prev)
{
    for(int tr = 0; tr < nr_tr; tr++) {
	struct track_ctx *cp = (struct track_ctx *)varray(tr_c_list, tr);
	cp->ellapse += nsec_from_prev / cp->nsec_per_tb;

	/* 経過時間が残っていて、トラック内のMMLが残っている限り、
	 * MML要素を処理する待ち時間があれば消費する。 */
	for(;;) {
	    printf(" track=%u curr=%u ellapse=%utb remain=%u+%utb\n",
		   cp->track_nr, cp->curr, cp->ellapse, cp->on_tb, cp->off_tb);

	    if(cp->curr >= cp->track->used)
		break;

	    if(cp->on_tb > 0) {
		if(cp->on_tb > cp->ellapse) {
		    /* まだゲートタイムに達していない */
		    cp->on_tb -= cp->ellapse;
		    cp->ellapse = 0;
		    break;
		} else {
		    /* ゲートタイムを超えた */
		    seq_note_off(cp);
		    cp->ellapse -= cp->on_tb;
		    cp->on_tb = 0;
		}
	    }

	    if(cp->on_tb == 0) {
		if(cp->off_tb > cp->ellapse) {
		    /* まだステップタイムに達していない */
		    cp->off_tb -= cp->ellapse;
		    cp->ellapse = 0;
		    break;
		} else {
		    /* ステップタイムを超えた */
		    cp->ellapse -= cp->off_tb;
		    seq_next_note(cp);
		}
	    }
	}
	printf(" track=%u curr=%u ellapse=%utb remain=%u+%utb\n",
	       cp->track_nr, cp->curr, cp->ellapse, cp->on_tb, cp->off_tb);
    }
}

void seq_play(void)
{
    for(int tr = 0; tr < nr_tr; tr++) {
	struct track_ctx *cp = (struct track_ctx *)varray(tr_c_list, tr);
	drv->set_channel(cp);
    }

    struct timespec prev_time, curr_time;
    clock_gettime(CLOCK_MONOTONIC, &prev_time);

    for(;;) {
	/* 前回ループ実行時からの経過時間を得る */
	clock_gettime(CLOCK_MONOTONIC, &curr_time);
	unsigned nsec_from_prev =
	    ((curr_time.tv_sec * nsec + curr_time.tv_nsec)
	     - (prev_time.tv_sec * nsec + prev_time.tv_nsec));
	printf("seq_play: %uns ellapsed\n", nsec_from_prev);
	seq_play_delta(nsec_from_prev);

	unsigned min_wait = -1;
	unsigned still_playing = 0;

	for(int tr = 0; tr < nr_tr; tr++) {
	    struct track_ctx *cp = (struct track_ctx *)varray(tr_c_list, tr);
	    /* 演奏状態を更新 */
	    if(cp->curr < cp->track->used) {
		still_playing |= 1;

		/* 次のイベントまでの待ち時間を更新 */
		unsigned tb_wait = (cp->on_tb > 0) ? cp->on_tb : cp->off_tb;
		unsigned nsec_wait = tb_wait * cp->nsec_per_tb;
		min_wait = (nsec_wait < min_wait) ? nsec_wait : min_wait;
	    }
	}

	/* 全トラックで演奏完了したならループ抜ける */
	if(!still_playing)
	    break;

	/* 時刻を記録し次のイベントまでウェイト。
	 * ウェイト時間は全トラックで最短で発生するもの */
	prev_time = curr_time;
	if(min_wait == 0)
	    continue;

	struct timespec wait_time;
	wait_time.tv_sec = min_wait / nsec;
	wait_time.tv_nsec = min_wait - wait_time.tv_sec * nsec;
	nanosleep(&wait_time, NULL);
	printf("waiting:  %uns\n", min_wait);
    }
}
