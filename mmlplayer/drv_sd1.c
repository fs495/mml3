#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>
#include "common.h"
#include "varray.h"

static const char * const spi_device = "/dev/spidev0.0";

enum iface_addr {
    ia_clock_enable,
    ia_reset,
    ia_ana_pow_down,
    ia_spk_gain,
    ia_hw_id,
    ia_intr,
    ia_6,
    ia_content_wdata,
    ia_seq_setting,
    ia_seq_vol,
    ia_seq_size,

    ia_synth_setting,
    ia_synth_vovol,
    ia_synth_fnum_high,
    ia_synth_fnum_low,
    ia_synth_key_on,
    ia_synth_chvol,
    ia_synth_xvb,
    ia_synth_int,
    ia_synth_frac,
    ia_synth_20,

    ia_ctrl_raddr,
    ia_ctrl_rdata,
    ia_seq_timeout_high,
    ia_seq_timeout_low,

    ia_master_vol,
    ia_soft_reset,
    ia_seq_delay,
    ia_lfo_reset,
    ia_power_sel,
    ia_resv_30,
    ia_resv_31,

    ia_eq0_wdata,
    ia_eq1_wdata,
    ia_eq2_wdata,
};

static const unsigned fnum_tablep[] = {
    357, 378, 401, 425, 450, 477,
    505, 535, 567, 601, 637, 674,
};

static int sd1_fd = -1;

static void sd1_open(void)
{
    sd1_fd = open(spi_device, O_RDWR);
    if(sd1_fd < 0) {
	fprintf(stderr, "Error: sd1_open: cannot open: %s\n", spi_device);
	exit(1);
    }

    uint8_t bits = 8;
    int ret = ioctl(sd1_fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if(ret < 0) {
	fprintf(stderr, "Error: ioctl(SPI_IOC_WR_BITS_PER_WORD, %d)\n", bits);
	exit(1);
    }

    unsigned speed = 1 * 1000 * 1000;
    ret = ioctl(sd1_fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if(ret < 0) {
	fprintf(stderr, "Error: ioctl(SPI_IOC_WR_MAX_SPEED_HZ, %d)\n",
		speed);
	exit(1);
    }

    unsigned mode = 0;
    ret = ioctl(sd1_fd, SPI_IOC_WR_MODE, &mode);
    if(ret < 0) {
	fprintf(stderr, "Error: ioctl(SPI_IOC_WR_MODE, %d)\n", mode);
	exit(1);
    }
}

static void sd1_write(uint8_t addr, uint8_t *data, unsigned len)
{
    struct spi_ioc_transfer tr[2];

    memset(tr, 0, sizeof(tr));
    tr[0].tx_buf = (uintptr_t)&addr;
    tr[0].len = 1;
    tr[1].tx_buf = (uintptr_t)data;
    tr[1].len = len;
    ioctl(sd1_fd, SPI_IOC_MESSAGE(2), tr);
}

static void sd1_iwrite(uint8_t reg, uint8_t data)
{
#ifdef SD1_TRACE
    printf("sd1_iwrite: reg 0x%02x = 0x%02x\n", reg, data);
#endif
    sd1_write(reg, &data, 1);
}

static void sd1_reset(void)
{
    /* negate /RST */

    /* select external 3.3V: DRV_SEL=1 */
    sd1_iwrite(ia_power_sel, 1);

    /* AP0 = 0 */
    sd1_iwrite(ia_ana_pow_down, 0x0e);
    usleep(1000);

    /* CLKE = 1 */
    sd1_iwrite(ia_clock_enable, 0x01);

    /* ALRST = 0 */
    sd1_iwrite(ia_reset, 0x00);

    /* SFTRST = 0xa3 */
    sd1_iwrite(ia_soft_reset, 0xa3);

    /* SFTRST = 0 */
    sd1_iwrite(ia_soft_reset, 0x00);
    
    /* wait 30ms */
    usleep(30000);

    /* AP1 = 0, AP3 = 0 */
    sd1_iwrite(ia_ana_pow_down, 0x04);

    /* wait 10us */
    usleep(10);

    /* AP2 = 0 */
    sd1_iwrite(ia_ana_pow_down, 0x00);

    /* master vol (0x33=+0dB) */
    sd1_iwrite(ia_master_vol, 0x33 << 2);

    /* interpolation */
    sd1_iwrite(ia_seq_delay, 0x3f);
    sd1_iwrite(ia_synth_20, 0x00);

    /* analog gain */
    sd1_iwrite(ia_spk_gain, 0x01);
   
    /* sequencer */
    sd1_iwrite(ia_seq_setting, 0xf6);
    usleep(21000);
    sd1_iwrite(ia_seq_setting, 0x00);
    sd1_iwrite(ia_seq_vol, 0xf8);
    sd1_iwrite(ia_seq_size, 0x00);

    sd1_iwrite(ia_seq_timeout_high, 0x40);
    sd1_iwrite(ia_seq_timeout_low, 0x00);
}

static void sd1_set_tones(void)
{
    printf("sd1_set_tones: %d tones\n", tones->used);
    unsigned buflen = 1 + TONE_PARAM_LEN * tones->used + 4;
    uint8_t buf[buflen];
    uint8_t *p = buf;

    *p++ = 0x80 + tones->used;
    for(int i = 0; i < tones->used; i++) {
	struct tone *tp = (struct tone *)varray(tones, i);
	memcpy(p, tp->param, TONE_PARAM_LEN);
	p += TONE_PARAM_LEN;
    }
    *p++ = 0x80;
    *p++ = 0x03;
    *p++ = 0x81;
    *p++ = 0x80;

    sd1_iwrite(ia_seq_setting, 0xf6);
    usleep(1000);
    sd1_iwrite(ia_seq_setting, 0x00);
    sd1_write(ia_content_wdata, buf, buflen);
}

static void sd1_set_channel(struct track_ctx *cp)
{
    sd1_iwrite(ia_synth_setting, cp->track_nr);
    /* key off, mute, EG reset */
    sd1_iwrite(ia_synth_key_on, 0x30 | cp->tone_nr);
    /* ChVol = 0x1f(+0dB), DIR_CV=1 */
    sd1_iwrite(ia_synth_chvol, (0x1f << 2) | 1);
    /* XVB = 0 */
    sd1_iwrite(ia_synth_xvb, 0x00);
    /* freq multiplier = 1.00 */
    sd1_iwrite(ia_synth_int, 1 << 3);
    sd1_iwrite(ia_synth_frac, 0);

}

/*
 * まんなかのド
 *   MIDI note 60 (12*5+0) = 261Hz = MML O3C = BLOCK 4 & FNUM 357
 * 時報
 *   MIDI note 69 (12*5+9) = 440Hz = MML O3A = BLOCK 4 & FNUM 601
 * 発音範囲
 *   O-1C~O-1B : Block 0 (MMLでは直接指定できないのでO0>とする)
 *   O0C~O0B : Block 1
 *   O6C~O6B : Block 7
 */
static void sd1_note_on(struct track_ctx *cp)
{
    unsigned octave = cp->note / 12;
    unsigned block = octave - 1;
    unsigned fnum = fnum_tablep[cp->note - octave * 12];

    sd1_iwrite(ia_synth_setting, cp->track_nr);
    sd1_iwrite(ia_synth_vovol, cp->volume);
    sd1_iwrite(ia_synth_fnum_high, (block & 7) | (fnum >> 7) << 3);
    sd1_iwrite(ia_synth_fnum_low, fnum & 0x7f);
    sd1_iwrite(ia_synth_key_on, 0x40 | cp->tone_nr);
}

static void sd1_note_off(struct track_ctx *cp)
{
    sd1_iwrite(ia_synth_setting, cp->track_nr);
    sd1_iwrite(ia_synth_key_on, cp->tone_nr);
}

struct drv_ops sd1_drv_ops = {
    .open = sd1_open,
    .reset = sd1_reset,
    .set_tones = sd1_set_tones,
    .set_channel = sd1_set_channel,
    .note_on = sd1_note_on,
    .note_off = sd1_note_off,
};
