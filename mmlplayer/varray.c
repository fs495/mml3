#include <stdint.h>
#include <string.h>

#include "varray.h"

enum {
    initial_size = 128,
    increment_size = 32
};

static struct varray *varray_alloc(unsigned n)
{
    struct varray *v = malloc(sizeof(struct varray));
    if(v == 0)
	abort();

    v->alloc = n;
    v->used = 0;
    v->ptr = malloc(n * sizeof(uintptr_t));
    if(v->ptr == 0)
	abort();
    return v;
}

struct varray *varray_new(void)
{
    return varray_alloc(initial_size);
}

struct varray *varray_clone(struct varray *v)
{
    struct varray *n = varray_alloc(v->alloc);
    memcpy(n->ptr, v->ptr, v->used * sizeof(uintptr_t));
    n->used = v->used;
    return n;
}

void varray_free(struct varray *v)
{
    free(v->ptr);
    free(v);
}

void varray_append(struct varray *v, uintptr_t item)
{
    if(v->used == v->alloc) {
	v->alloc += increment_size;
	v->ptr = realloc(v->ptr, v->alloc * sizeof(uintptr_t));
	if(v->ptr == 0)
	    abort();
    }
    v->ptr[v->used++] = item;
}

void varray_merge(struct varray *v, struct varray *a)
{
    if(v->used + a->used >= v->alloc) {
	v->alloc = (v->used + a->used + v->alloc - 1) / v->alloc * v->alloc;
	v->ptr = realloc(v->ptr, v->alloc * sizeof(uintptr_t));
	if(v->ptr == 0)
	    abort();
    }
    memcpy(v->ptr + v->used, a->ptr, a->used * sizeof(uintptr_t));
    v->used += a->used;
}

