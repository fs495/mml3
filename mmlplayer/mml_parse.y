%{
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "varray.h"
#include "common.h"
#include "y.tab.h"

/* スキャナー&パーサー */
extern int yyline;
extern int yylex(void);
extern void yyerror(const char *msg);

/*----------------------------------------------------------------------*/

#define YYPRINT(file, type, value)   yyprint(file, type, value)

static void yyprint(FILE *file, int type, YYSTYPE value)
{
    switch(type) {
    case NUMBER:
	fprintf(file, "%u", value.number);
	break;
    case MACRO_NAME:
	fprintf(file, "\"%s\"", value.name);
	break;
    case DIRECTIVE_NAME:
	fprintf(file, "\"%s\"", value.name);
	break;
    }
}

static void macro_register(const char *mname, struct varray *body)
{
    /* すでに登録済みならば上書き */
    for(int i = 0; i < macros->used; i++) {
	struct identifier *ip = (struct identifier *)varray(macros, i);
	if(strcmp(ip->name, mname) == 0) {
	    ip->array = body;
	    return;
	}
    }
    /* 見つからない場合は新規登録 */
    struct identifier *ip = malloc(sizeof(struct identifier));
    ip->name = strdup(mname);
    ip->array = body;
    varray_append(macros, (uintptr_t)ip);
}

static struct varray *macro_lookup(const char *mname)
{
    for(int i = 0; i < macros->used; i++) {
	struct identifier *ip = (struct identifier *)varray(macros, i);
	if(strcmp(ip->name, mname) == 0)
	    return ip->array;
    }
    return NULL;
}

static void print_macro_list(void)
{
    printf("List of macros:\n");
    for(int i = 0; i < macros->used; i++) {
	struct identifier *ip = (struct identifier *)varray(macros, i);
	printf(" %s at %p len %u\n", ip->name, ip->array, ip->array->used);
	print_mmle_list(ip->array);
    }
}

static void parse_directive(const char *dname, struct varray *a)
{
    if(strcmp(dname, "#tone") == 0) {
	/* トーン宣言 */
	if(a->used < 1 + TONE_PARAM_LEN) {
	    printf("Error: %s requires %d numbers (actually %d) at line %d\n",
		   dname, 1 + TONE_PARAM_LEN, a->used, yyline);
	    return;
	}
	struct tone *tp = malloc(sizeof(struct tone));
	tp->decl_num = varray(a, 0);
	for(int i = 0; i < TONE_PARAM_LEN; i++)
	    tp->param[i] = varray(a, i + 1);
	varray_append(tones, (uintptr_t)tp);

    } else {
	printf("Warning: ignored unknown directive '%s' at line %d\n", dname, yyline);
    }
}

static void print_track_list(void)
{
    printf("List of tracks:\n");
    for(int i = 0; i < tracks->used; i++) {
	struct varray *ip = (struct varray *)varray(tracks, i);
	printf(" track %d at %p len %u\n", i, ip, ip->used);
	print_mmle_list(ip);
    }
}

void parse_init(void)
{
    null_mmle = varray_new();
    macros = varray_new();
    tones = varray_new();
    tracks = varray_new();
}

void parse_fini(void)
{
    print_macro_list();
    print_track_list();
}

/*----------------------------------------------------------------------*/
%}

%union {
    uintptr_t number;
    struct varray *array;
    const char *name;
}

%token	<number>	NUMBER
%token	<name>		MACRO_NAME
%token	<name>		DIRECTIVE_NAME

%type	<array>		number_list literal_list mml_elem
%type	<number>	note rest base_note note_offset length tie

%start top
%%

top: 	  top statement
	| statement
	| /*empty */
	;

statement:
	  directive
	| macro_def
	| track_def
	;

directive:
	  DIRECTIVE_NAME number_list ';' { parse_directive($1, $2); }
	| DIRECTIVE_NAME ';'		 { parse_directive($1, varray_new()); }
	;

number_list:
	  number_list ',' NUMBER	{ varray_append($1, $3); $$ = $1; }
	| NUMBER			{ $$ = varray_new_with($1); }
	;

macro_def:
	  MACRO_NAME '=' literal_list ';' { macro_register($1, $3); }
	;

track_def:
	  literal_list ';'	{ varray_append(tracks, (uintptr_t)$1); }
	;


literal_list
	: literal_list mml_elem	{ varray_merge($1, $2); $$ = $1; }
	| mml_elem		{ $$ = $1; }
	;

mml_elem: note		{ $$ = varray_new_with($1); }
	| rest		{ $$ = varray_new_with($1); }
	| 't' NUMBER {
	    if($2 == 0) {
		printf("Error: 't' must be greater than 0\n");
		$$ = null_mmle;
	    } else {
		$$ = varray_new_with(gen_mmle('t', $2));
	    }
	  }
	| 'l' NUMBER {
	    if($2 == 0) {
		printf("Error: 'l' must be greater than 0\n");
		$$ = null_mmle;
	    } else {
		$$ = varray_new_with(gen_mmle('l', $2));
	    }
	  }
	| 'q' NUMBER {
	    if($2 == 0) {
		printf("Error: 'q' must be greater than 0\n");
		$$ = null_mmle;
	    } else {
		$$ = varray_new_with(gen_mmle('q', $2));
	    }
	  }
	| 'v' NUMBER	{ $$ = varray_new_with(gen_mmle('v', $2)); }
	| 'o' NUMBER	{ $$ = varray_new_with(gen_mmle('o', $2)); }
	| '<'		{ $$ = varray_new_with(gen_mmle('<', +1)); }
	| '>'		{ $$ = varray_new_with(gen_mmle('>', -1)); }
	| '@' NUMBER	{ $$ = varray_new_with(gen_mmle('@', $2)); }

	| '|' ':' NUMBER	{ $$ = varray_new_with(gen_mmle(REPEAT_START, $3)); }
	| '|' ':'		{ $$ = varray_new_with(gen_mmle(REPEAT_START, 2)); }
	| '/'			{ $$ = varray_new_with(gen_mmle(REPEAT_EXIT, 0)); }
	| ':' '|'		{ $$ = varray_new_with(gen_mmle(REPEAT_END, 0)); }

	| MACRO_NAME	{
	    struct varray *vp = macro_lookup($1);
	    if(vp == 0)
		printf("Error: macro '%s' not defined at line %d\n", $1, yyline);
	    else
		$$ = varray_clone(vp);
	  }
	;

note:	  base_note note_offset length tie {
	      $$ = note_mmle(($1 + $2) % 12, $4, $3);
	  };

rest:     'r' length {
	      $$ = note_mmle('r', 0, $2);
	  };

base_note:
	  'c' { $$ = 0; }
	| 'd' { $$ = 2; }
	| 'e' { $$ = 4; }
	| 'f' { $$ = 5; }
	| 'g' { $$ = 7; }
	| 'a' { $$ = 9; }
	| 'b' { $$ = 11; }
	;

note_offset:
	  '#'		{ $$ = 1; }
	| '+' 		{ $$ = 1; }
	| '-'		{ $$ = 11; }
	| /* empty */	{ $$ = 0; }
	;

length:	  NUMBER	{ $$ = WN_TB / $1; }
	| NUMBER '.'	{ $$ = WN_TB / $1 * 3 / 2; }
	| '.'		{ $$ = LEN_DEFAULT32; }
	| /* empty */	{ $$ = LEN_DEFAULT; }
	;

tie:	  '&'		{ $$ = FLAG_FULL_GATE; }
	| /* empty */	{ $$ = 0; }
	;
