#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "varray.h"
#include "common.h"

extern struct drv_ops sd1_drv_ops;
extern struct drv_ops null_drv_ops;

struct drv_ops *drv = &sd1_drv_ops;
/* struct drv_ops *drv = &null_drv_ops; */

extern FILE *yyin;
extern void yyrestart(FILE *fp);
extern int yyparse(void);

int main(int argc, char **argv)
{
#if YYDEBUG
    yydebug = 1;
#endif
    if(argc > 1) {
	yyin = fopen(argv[1], "r");
	if(yyin == NULL) {
	    fprintf(stderr, "Error: cannot open MML: %s\n", argv[1]);
	    exit(1);
	}
	yyrestart(yyin);
    } else {
	yyin = stdin;
    }

    parse_init();
    yyparse();
    parse_fini();

    seq_init();
    drv->open();
    drv->reset();
    if(tones->used > 0)
	drv->set_tones();
    seq_play();

    return 0;
}
