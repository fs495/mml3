#include <stdint.h>
#include "common.h"

static void null_open(void)
{
    /* do nothing */
}

static void null_write(uint8_t addr, uint8_t *data, unsigned len)
{
    /* do nothing */
}

static void null_iwrite(uint8_t reg, uint8_t data)
{
    /* do nothing */
}

static void null_reset(void)
{
    /* do nothing */
}

static void null_set_tones(void)
{
    /* do nothing */
}

static void null_set_channel(struct track_ctx *cp)
{
    /* do nothing */
}

static void null_note_on(struct track_ctx *cp)
{
    /* do nothing */
}

static void null_note_off(struct track_ctx *cp)
{
    /* do nothing */
}

struct drv_ops null_drv_ops = {
    .open = null_open,
    .reset = null_reset,
    .set_tones = null_set_tones,
    .set_channel = null_set_channel,
    .note_on = null_note_on,
    .note_off = null_note_off,
};
