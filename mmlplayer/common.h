/*----------------------------------------------------------------------
 * 音色
 */
enum tone_param {
    TONE_PARAM_LEN = 2 + 7 * 4, /* 音色定義データ長 */
    MAX_TONE = 16,
};

struct tone {
    unsigned decl_num;
    uint8_t param[TONE_PARAM_LEN];
};

extern struct varray *tones;

/*----------------------------------------------------------------------
 * ディレクティブまたはマクロ
 * arrayは、ディレクティブの場合は数値、マクロの場合はMML要素を指す
 */
struct identifier {
    char *name;
    struct varray *array;
};

extern struct varray *macros;

/*----------------------------------------------------------------------
 * トラック
 */
extern struct varray *tracks;

enum {
    max_track = 16,
    max_loop_depth = 8,
};

struct track_ctx {
    struct varray *track;
    unsigned track_nr;
    unsigned curr;

    unsigned on_tb, off_tb;
    unsigned ellapse;

    unsigned note; /* 0(C) ... 11(B#) */
    unsigned octave;
    unsigned volume;

    unsigned nsec_per_tb;
    unsigned default_tb;
    unsigned gate_ratio;

    unsigned stack_dep;
    unsigned pos_stack[max_loop_depth];
    unsigned cnt_stack[max_loop_depth];

    unsigned tone_nr;
};

/*----------------------------------------------------------------------
 * MML要素
 */
enum mmle_type_consts {
    REPEAT_START = 128,
    REPEAT_EXIT,
    REPEAT_END
};

enum mmle_flag_consts {
    FLAG_FULL_GATE = 1, /* スラーまたはタイ */
};

enum mmle_len_consts {
    WN_TB = 480 * 4,
    LEN_DEFAULT = 65535, /* その時のデフォルトのステップタイム */
    LEN_DEFAULT32 = 65534, /* 付点付きデフォルトステップタイム */
};

/*
 * MML要素のフォーマット
 *	MSB            LSB
 *	|type|flag|len_len|
 *	| 8  | 8  | 16    |
 */
static inline uintptr_t note_mmle(uint8_t type, uint8_t flag, uint16_t val) {
    return (type << 24) | (flag << 16) | val;
}
static inline uintptr_t gen_mmle(uint8_t type, uint16_t val) {
    return note_mmle(type, 0, val);
}
static inline uint8_t mmle_type(uintptr_t mmle) { return mmle >> 24; }
static inline uint8_t mmle_flag(uintptr_t mmle) { return mmle >> 16; }
static inline uint16_t mmle_val(uintptr_t mmle) { return mmle; }

extern const char *note_to_str[];
extern struct varray *null_mmle;

void print_mmle_list(struct varray *a);

/*----------------------------------------------------------------------
 * MMLパーサー
 */
extern void parse_init(void);
extern void parse_fini(void);

extern int yylex(void);

/*----------------------------------------------------------------------
 * シーケンサー
 */
extern void seq_init(void);
extern void seq_play(void);

/*----------------------------------------------------------------------
 * ドライバ
 */
struct drv_ops {
    void (*open)(void);
    void (*reset)(void);
    void (*set_tones)(void);
    void (*set_channel)(struct track_ctx *cp);
    void (*note_on)(struct track_ctx *cp);
    void (*note_off)(struct track_ctx *cp);
};

extern struct drv_ops *drv;
