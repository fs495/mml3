%{
#include <stdint.h>
#include <stdio.h>
#include <ctype.h>
#include "y.tab.h"

extern void yyerror(const char *s);

int yyline = 1;
%}

ALPHA	[_A-Za-z]
ALNUM	[_A-Za-z0-9]
IDENT	{ALPHA}{ALNUM}*

%%

\${IDENT} {
	yylval.name = strdup(yytext);
	return MACRO_NAME;
}

#{IDENT} {
	yylval.name = strdup(yytext);
	return DIRECTIVE_NAME;
}

[0-9]+ {
	/* sscanf(yytext, "%d", &yylval.number); */
	yylval.number = atoi(yytext);
	return NUMBER;
}

[a-zA-Z]	return tolower(yytext[0]);

[=;,]		return yytext[0];
[&.\+\-#<>@]	return yytext[0];
[/|:]		return yytext[0];

\/\/.*		/* eat up comment */

[ \t]+		/* eat up white space */

\n		yyline++;

.		yyerror("unexpected input");

%%
void yyerror(const char *msg)
{
    fprintf(stderr, "Error: %s in line %d: token \"%s\"\n", msg, yyline, yytext);
}

int yywrap(void)
{
    return 1;
}
