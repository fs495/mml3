/*
 * SMF情報の参照元: https://sites.google.com/site/yyagisite/material/smfspec
 */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "varray.h"
#include "common.h"

struct smf_header smfh;

struct varray *nlist;

/*----------------------------------------------------------------------
 */

static void smf_to_mml(FILE *fin)
{
    nlist = varray_new();
    load_smf(fin);
    generate_mml();
}

int main(int argc, char *argv[])
{
    FILE *fin = stdin;
    if(argc > 1) {
	fin = fopen(argv[1], "rb");
	if(fin == NULL) {
	    fprintf(stderr, "Error: %s: cannot open\n", argv[1]);
	    exit(1);
	}
    }
    smf_to_mml(fin);
    fclose(fin);
    return 0;
}
