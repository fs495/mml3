#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "varray.h"
#include "common.h"

enum {
    /*
     * ノートオンのクオンタイズ閾値。
     * ノートオン時間を四分音符の何分の一の単位に丸めるか、
     * つまりtpqn / quant_thre_note_onで割り切れる数に丸める。
     * e.g. 8の場合、4*8=32分音符単位に丸める。tpqn=480の場合は60の倍数に丸める。
     */
    quant_thre_note_on = 32,

    /*
     * ノートオフのクオンタイズ閾値。
     */
    quant_thre_note_off = 4,
};

enum {
    debug_split = 0,
    debug_convert = 1,
};

struct mml_track {
    struct varray *note_list;
    struct note_record *curr_np;
    uint8_t midi_chan, smf_track;
};

static void print_note_record(struct note_record *np)
{
    printf("time=%d-%d midi_chan=%d note=%d velo=%d smf_track=%d\n",
	   np->start, np->end,
	   np->chan+1, np->note, np->velocity, np->smf_track);
}


/*----------------------------------------------------------------------
 * MML変換
 */

static struct varray *
split_into_mml_tracks(unsigned midi_chan, unsigned smf_track)
{
    struct varray *mtlist = varray_new();
    if(debug_split)
	printf("## midi_chan=%d, smf_track=%d\n", midi_chan, smf_track);

    for(unsigned i = 0; i < nlist->used; i++) {
	/* 条件に合うノート情報以外は無視 */
	struct note_record *np = (struct note_record *)varray(nlist, i);
	if(np->chan != midi_chan || np->smf_track != smf_track)
	    continue;
	if(debug_split)
	    print_note_record(np);

	for(unsigned j = 0; j < mtlist->used; j++) {
	    struct mml_track *mtp = (struct mml_track *)varray(mtlist, j);
	    /* ノートオフ中のMMLトラックはスキップ */
	    if(mtp->curr_np == 0)
		continue;

	    /* 注目ノートのノートオンが、すでにノートオン中のMMLトラックの
	     * ノートオフより後かどうか調べる */
	    if(np->start >= mtp->curr_np->end) {
		if(debug_split)
		    printf(" note_off: mml_track=%d: %d-%d\n",
			   j, mtp->curr_np->start, mtp->curr_np->end);
		mtp->curr_np = 0;
	    } else {
		if(debug_split)
		    printf(" note_keep: mml_track=%d: %d-%d\n",
			   j, mtp->curr_np->start, mtp->curr_np->end);
	    }
	}

	/* 空きMMLトラックを検索 */
	int found = 0;
	for(unsigned j = 0; j < mtlist->used; j++) {
	    struct mml_track *mtp = (struct mml_track *)varray(mtlist, j);
	    if(mtp->curr_np != 0)
		continue;

	    /* 空きMMLトラックが見つかったら、注目ノートを追加 */
	    varray_append(mtp->note_list, (uintptr_t)np);
	    mtp->curr_np = np;
	    found = 1;
	    if(debug_split) {
		printf(" reuse mml_track=%d: ", j);
		print_note_record(np);
	    }
	    break;
	}
	
	if(!found) {
	    /* 空きMMLトラックが見つからなかったため、新しいものを追加 */
	    if(debug_split) {
		printf(" added mml_track=%d: ", mtlist->used);
		print_note_record(np);
	    }
	    struct mml_track *mtp = malloc(sizeof(struct mml_track));
	    mtp->note_list = varray_new_with((uintptr_t)np);
	    mtp->curr_np = np;
	    mtp->midi_chan = midi_chan;
	    mtp->smf_track = smf_track;
	    varray_append(mtlist, (uintptr_t)mtp);
	}

	if(debug_split)
	    printf("\n");
    }

    if(mtlist->used > 0)
	printf("## midi_chan=%d, smf_track=%d: %d mml_tracks\n",
	       midi_chan+1, smf_track, mtlist->used);

    return mtlist;
}

static void out_rest(int len)
{
    // TODO: 連休符
    int unit = smfh.tpqn * 4;
    int div = 0;

    while(len > 0) {
	if(len >= unit) {
	    printf("r%d", 1 << div);
	    len -= unit;
	    if(len == unit / 2) {
		printf(".");
		break;
	    }
	    continue;
	}
	unit >>= 1;
	div += 1;
    }
}

static const char *note_to_str[] = {
    "c", "c+", "d", "d+", "e",
    "f", "f+", "g", "g+", "a", "a+", "b",
};

static void out_note(int len, struct note_record *np)
{
    int oct = np->note / 12;
    int key = np->note % 12;

    int unit = smfh.tpqn * 4;
    int div = 0;

    // TODO: 連符

    printf("o%d", oct - 2);
    while(len > 0) {
	if(len >= unit) {
	    printf("%s%d", note_to_str[key], 1 << div);
	    len -= unit;
	    if(len == unit / 2) {
		printf(".");
		break;
	    }
	    continue;
	}
	unit >>= 1;
	div += 1;
    }
}

static void change_gate(unsigned gate_ratio)
{
    printf("q%d", gate_ratio);
}

static void convert_mml_track(struct mml_track *mtp)
{
    if(debug_convert)
	printf("//### mml_track start midi_chan=%d, smf_track=%d\n",
	       mtp->midi_chan+1, mtp->smf_track);

    int curr_time = 0;
    int quant = smfh.tpqn / quant_thre_note_on;
    int startq, endq, next_startq, gate_ratio = 128;

    for(unsigned n = 0; n < mtp->note_list->used; n++) {
	struct note_record *curr = (struct note_record *)varray(mtp->note_list, n);
	if(debug_convert) {
	    printf("\n// [%d/%d] ", n, mtp->note_list->used);
	    print_note_record(curr);
	}

	startq = curr->start / quant * quant;
	if(n < mtp->note_list->used - 1) {
	    struct note_record *next = (struct note_record *)varray(mtp->note_list, n + 1);
	    next_startq = next->start / quant * quant;
	} else {
	    next_startq = endq;
	}

	/* ノートオンまでに時間が飛んでいる場合は休符を出力 */
	if(startq > curr_time)
	    out_rest(startq - curr_time);

	/* 現在のゲート比設定からノートオフ時間を仮算出 */
	endq = startq + (next_startq - startq) * gate_ratio / 128;

	if((endq >= curr->end && (endq - curr->end) <= smfh.tpqn / quant_thre_note_off)
	   || (endq < curr->end && (curr->end - endq) <= smfh.tpqn / quant_thre_note_off)) {
	    /* ゲート比変えなくても大丈夫 */
	    printf("// case1: %d->%d->%d\n", startq, endq, next_startq);
	    out_note(next_startq - startq, curr);
	    curr_time = next_startq;
	} else {
	    if(next_startq - startq >= 2 * smfh.tpqn) {
		/* ゲート比というよりは長い休符と考えるほうが良さそう */
		printf("// case2: %d->%d->%d\n", startq, endq, next_startq);
		change_gate(128);
		out_note(endq - startq, curr);
		curr_time = endq;
	    } else {
		printf("// case3: %d->%d->%d\n", startq, endq, next_startq);
		gate_ratio = (endq - startq) * 128 / (next_startq - startq);
		change_gate(gate_ratio);
		out_note(next_startq - startq, curr);
		curr_time = next_startq;
	    }
	}
    }
}


/*
 * MIDIチャンネルｰ>ノートオン時刻ｰ>SMFトラック->ノート番号
 * の順番でソートするための比較関数
 */
static int note_comparator(const void *va, const void *vb)
{
    struct note_record *a = *(struct note_record **)va;
    struct note_record *b = *(struct note_record **)vb;

#define COMPARE(m) a->m != b->m ? a->m - b->m
    return COMPARE(chan)
	: COMPARE(start)
	: COMPARE(smf_track)
	: COMPARE(note)
	: 0;
#undef COMPARE
}

void generate_mml(void)
{
    /* ノート情報のリストをソート */
    qsort(nlist->ptr, nlist->used, sizeof(uintptr_t), note_comparator);

#undef PRINT_NOTES
#ifdef PRINT_NOTES
    for(unsigned i = 0; i < nlist->used; i++) {
	struct note_record *np = (struct note_record *)varray(nlist, i);
	print_note_record(np);
    }
#endif

    unsigned midi_chan, smf_track;
    for(midi_chan = 0; midi_chan < 16; midi_chan++)
	for(smf_track = 0; smf_track < smfh.nr_smf_tracks; smf_track++) {
	    /* 対象のSMFトラックを、0個以上のMMLトラックに分離する */
	    struct varray *mtlist = split_into_mml_tracks(midi_chan, smf_track);
	    for(unsigned mt = 0; mt < mtlist->used; mt++) {
		struct mml_track *mtp = (struct mml_track *)varray(mtlist, mt);
		convert_mml_track(mtp);
	    }
	}
		
}
