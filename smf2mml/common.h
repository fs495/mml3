struct smf_header {
    uint8_t type[4];
    uint32_t header_len;
    uint16_t format, nr_smf_tracks, tpqn;
};
extern struct smf_header smfh;

struct note_record {
    uint32_t start, end;
    uint8_t chan, note, velocity, smf_track;
};
extern struct varray *nlist;

void load_smf(FILE *fin);
void generate_mml(void);
