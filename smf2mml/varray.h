#include <stdlib.h>
#include <stdint.h>

struct varray {
    uintptr_t *ptr;
    unsigned used, alloc;
};

struct varray *varray_new(void);
struct varray *varray_clone(struct varray *v);

void varray_free(struct varray *v);

void varray_append(struct varray *v, uintptr_t item);

void varray_merge(struct varray *v, struct varray *a);

static inline uintptr_t varray(struct varray *v, unsigned n) {
    if(n >= v->used)
	abort();
    return v->ptr[n];
}

static inline struct varray *varray_new_with(uintptr_t item) {
    struct varray *v = varray_new();
    varray_append(v, item);
    return v;
}
