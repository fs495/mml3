#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "varray.h"
#include "common.h"

struct smf_track_info {
    uint32_t atime;
    unsigned smf_track;
    unsigned used_midi_channel; /* そのSMFトラックで1度でも使用されるMIDIチャンネル */
    unsigned nr_midi_event, nr_sysex, nr_meta, nr_unknown_event;
    uint32_t n_on_time[16][128]; /* ノートオン時刻 */
    uint8_t n_on_count[16][128]; /* MIDIチャンネルごとのノートオンカウント */
    uint8_t n_on_smftr[16][128]; /* ノートオンさせたSMFトラック番号 */
    uint8_t n_on_velocity[16][128]; /* ノートオン時のベロシティ */
};
static struct smf_track_info tt;

enum {
    debug_meta = 0,
};

/*----------------------------------------------------------------------
 * ユーティリティー
 */

static uint32_t unpack_u4(uint8_t *p)
{
    return (p[0] << 24) | (p[1] << 16) | (p[2] << 8) | p[3];
}

static uint16_t unpack_u2(uint8_t *p)
{
    return (p[0] << 8) | p[1];
}

static uint32_t unpackv(uint8_t *buf, uint32_t * const offset)
{
    uint32_t v = buf[*offset] & 0x7f;
    if((buf[(*offset)++] & 0x80) == 0)
	return v;
    v = (v << 7) | (buf[*offset] & 0x7f);
    if((buf[(*offset)++] & 0x80) == 0)
	return v;
    v = (v << 7) | (buf[*offset] & 0x7f);
    if((buf[(*offset)++] & 0x80) == 0)
	return v;
    v = (v << 7) | (buf[*offset] & 0x7f);
    (*offset)++;
    return v;
}

/*----------------------------------------------------------------------
 * SMFヘッダ
 */

static int read_smf_header(FILE *fin)
{
    uint8_t buf[14];

    fread(buf, 1, sizeof(buf), fin);
    if(buf[0] != 'M' || buf[1] != 'T' || buf[2] != 'h' || buf[3] != 'd') {
	fprintf(stderr, "Error: missing SMF header signature: not SMF?\n");
	return -1;
    }
    memcpy(smfh.type, buf+0, 4);

    smfh.header_len = unpack_u4(buf+4);
    if(smfh.header_len != 6) {
	fprintf(stderr, "Error: SMF header length != 6: unknown format?\n");
	return -1;
    }    

    smfh.format = unpack_u2(buf+8);
    if(smfh.format != 0 && smfh.format != 1) {
	fprintf(stderr, "Error: SMF header format unknown\n");
	return -1;
    }
    
    smfh.nr_smf_tracks = unpack_u2(buf+10);

    smfh.tpqn = unpack_u2(buf+12);
    if(smfh.tpqn & 0x8000) {
	fprintf(stderr, "Error: unsupported time code: 0x%x\n", smfh.tpqn);
	return -1;
    }
    return 0;
}

/*----------------------------------------------------------------------
 * SMFトラック(MIDIイベント)
 */

static void note_on(uint8_t status, uint8_t note, uint8_t velocity)
{
    uint8_t chan = status & 0x0f;
    /* printf("\tnote_on: time %u: chan %d note %d\n", tt.atime, chan, note); */

    if(tt.n_on_count[chan][note] == 0) {
	/* 最初のノートオン */
	tt.n_on_time[chan][note] = tt.atime;
	tt.n_on_velocity[chan][note] = velocity;
	tt.used_midi_channel |= 1 << chan;
    }
    tt.n_on_count[chan][note]++;
}

static void note_off(uint8_t status, uint8_t note, uint8_t velocity)
{
    uint8_t chan = status & 0x0f;
    /* printf("\tnote_off: chan %d note %d\n", chan, note); */
    
    tt.n_on_count[chan][note]--;
    if(tt.n_on_count[chan][note] == 0) {
	/* 最後のノートオフ */
	struct note_record *np = malloc(sizeof(struct note_record));
	np->start= tt.n_on_time[chan][note];
	np->end  = tt.atime;
	np->chan = chan;
	np->note = note;
	np->velocity = tt.n_on_velocity[chan][note];
	np->smf_track = tt.smf_track;
	varray_append(nlist, (uintptr_t)np);
	/* printf("\tnote_off: chan %d note %d time %u-%u\n", chan, note,
	   tt.n_on_time[chan][note], tt.atime); */
    }
}

static void read_midi_event(uint8_t *buf, uint8_t status, uint32_t *offset)
{
    /* printf("\tmidi: status %02x at offset %u\n", status, *offset); */
    switch(status & 0xf0) {
    case 0x80:
	note_off(status, buf[*offset], 0);
	*offset += 2;
	break;
    case 0x90:
	if(buf[*offset+1] == 0)
	    note_off(status, buf[*offset], 0);
	else
	    note_on(status, buf[*offset], buf[*offset+1]);
	*offset += 2;
	break;
    case 0xa0:
	*offset += 2;
	break;
    case 0xb0:
	*offset += 2; /* TODO: OMNI OFF時のMONO指定だけ+3する */
	break;
    case 0xc0:
	*offset += 1;
	break;
    case 0xd0:
	*offset += 1;
    case 0xe0:
	*offset += 2;
    default:
	fprintf(stderr, "Error: unknown MIDI event: 0x%x\n", status);
	exit(1);
    }
}

/*----------------------------------------------------------------------
 * SMFトラック (MIDIイベント以外)
 */

static void read_sysex(uint8_t *buf, uint32_t *offset)
{
    uint8_t first = buf[*offset];
    if(first == 0xf0 || first == 0xf7) {
	(*offset)++;
	uint32_t x = unpackv(buf, offset);
	(*offset) += x;
	printf("\tsysex: %u\n", x);
    } else {
	fprintf(stderr, "Error: unknown system exclusive: 0x%x\n", first);
	exit(1);
    }
}

static const char * const key_sig_base[2][13] = {
    {/* Major */
	"G♭", "D♭", "A♭", "E♭", "B♭", "F",
	"C",
	"G", "D", "A", "E", "B", "F♯",
    },
    {/* Minor */
	"E♭", "B♭", "F", "C", "G", "D",
	"A",
	"E", "B", "F♯", "C♯", "G♯", "D♯",
    }
};

static const char * const key_sig_suffix[2] = {
    "Major", "Minor"
};

static void read_meta_event(uint8_t *buf, uint32_t *offset)
{
    uint8_t type = buf[*offset + 1];
    (*offset) += 2;
    uint32_t len = unpackv(buf, offset);
    buf += *offset;

    if(debug_meta) {
	switch(type) {
	case 0x00:
	    printf("\tmeta: sequence number: %d\n", unpack_u4(buf));
	    break;
	case 0x01:
	    printf("\tmeta: text: %*s\n", len, buf);
	    break;
	case 0x02:
	    printf("\tmeta: copyright: %*s\n", len, buf);
	    break;
	case 0x03:
	    printf("\tmeta: seq/track name: %*s\n", len, buf);
	    break;
	case 0x04:
	    printf("\tmeta: instrument name: %*s\n", len, buf);
	    break;
	case 0x05:
	    printf("\tmeta: lyrics: %*s\n", len, buf);
	    break;
	case 0x06:
	    printf("\tmeta: marker: %*s\n", len, buf);
	    break;
	case 0x07:
	    printf("\tmeta: queue point: %*s\n", len, buf);
	    break;
	case 0x08:
	    printf("\tmeta: program name: %*s\n", len, buf);
	    break;
	case 0x09:
	    printf("\tmeta: device name: %*s\n", len, buf);
	    break;

	case 0x20:
	    printf("\tmeta: MIDI channel prefix: %d\n", buf[0]);
	    break;
	case 0x21:
	    printf("\tmeta: output port: %d\n", buf[0]);
	    break;
	case 0x2f:
	    printf("\tmeta: end of track\n");
	    break;

	case 0x51:
	    printf("\tmeta: tempo: %uus per MIDI quarter note\n",
		   (buf[0] << 16) | (buf[1] << 8) | buf[2]);
	    break;
	case 0x54:
	    printf("\tmeta: TODO: type=0x%x len=%u\n", type, len);
	    break;
	case 0x58:
	    printf("\tmeta: time signature: %d/%d\n", buf[0], 1 << buf[1]);
	    break;
	case 0x59:
	    printf("\tmeta: key signature: %s %s\n",
		   key_sig_base[buf[1]][(buf[0] + 6) & 0xff],
		   key_sig_suffix[buf[1]]);
	    break;

	case 0x7f:
	    printf("\tmeta: sequencer-specific: len=%u\n", len);
	    break;
	default:
	    printf("\tmeta: unknown: type=0x%x len=%u\n", type, len);
	    break;
	}
    }

    (*offset) += len;
}

static uint32_t read_smf_track_header(FILE *fin)
{
    long fpos = ftell(fin);
    uint8_t buf[8];
    fread(buf, 1, sizeof(buf), fin);
    if(buf[0] != 'M' || buf[1] != 'T' || buf[2] != 'r' || buf[3] != 'k') {
	fprintf(stderr, "Error: bad track format at 0x%lx\n", fpos);
	exit(1);
    }
    return unpack_u4(buf+4);
}

static void read_smf_track_body(FILE *fin, uint32_t trlen)
{
    uint8_t buf[trlen];
    long fpos = ftell(fin);
    uint32_t offset = 0;
    uint8_t running_status;
    fread(buf, 1, trlen, fin);

    /* メモリに読み込んだトラック全体を順番に走査する */
    while(offset < trlen) {
	uint32_t prev_offset = offset;
	uint32_t delta = unpackv(buf, &offset);
	uint8_t first = buf[offset];

	tt.atime += delta;

	if((first & 0xf0) < 0x80) {
	    /* ランニングステータスありのMIDIイベント */
	    read_midi_event(buf, running_status, &offset);
	    tt.nr_midi_event++;
	} else if((first & 0xf0) < 0xf0) {
	    /* (ランニングステータスなしの)MIDIイベント */
	    offset++;
	    running_status = first;
	    read_midi_event(buf, running_status, &offset);
	    tt.nr_midi_event++;
	} else if(first == 0xf0 || first == 0xf7) {
	    /* システムエクスクルーシブ */
	    read_sysex(buf, &offset);
	    tt.nr_sysex++;
	} else if(first == 0xff) {
	    /* メタイベント */
	    read_meta_event(buf, &offset);
	    tt.nr_meta++;
	} else {
	    fprintf(stderr, "Error: unknown event 0x%02x at 0x%lx\n",
		    first, fpos + prev_offset);
	    tt.nr_unknown_event++;
	}
    }
}

void load_smf(FILE *fin)
{
    /* SMFヘッダ */
    if(read_smf_header(fin) != 0) {
	fprintf(stderr, "Error: cannot read SMF header successfully\n");
	return;
    }
    printf("SMF header\n");
    printf("\tSMF header length: %d bytes\n", smfh.header_len);
    printf("\tSMF format: %d\n", smfh.format);
    printf("\tnumber of SMF tracks: %d\n", smfh.nr_smf_tracks);
    printf("\ttime division: %d ticks per quater note\n", smfh.tpqn);

    for(int tr = 0; tr < smfh.nr_smf_tracks; tr++) {
	/* SMFトラックヘッダ */
	long fpos = ftell(fin);
	uint32_t trlen = read_smf_track_header(fin);
	printf("SMF track %d: starts at 0x%lx, length %u\n", tr, fpos, trlen);

	/* SMFトラック本体 */
	memset(&tt, 0, sizeof(tt));
	tt.smf_track = tr;
	read_smf_track_body(fin, trlen);
	printf("\t%d MIDI events, %d system exclusives, "
	       "%d meta events, %d unknown events\n",
	       tt.nr_midi_event, tt.nr_sysex, tt.nr_meta, tt.nr_unknown_event);
    }
}

