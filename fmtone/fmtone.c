#include <stdint.h>
#include "common.h"

struct tone_param tp;

void pack(void)
{
    tp.raw[0] = tp.common[cp_bo];
    tp.raw[1] = (tp.common[cp_lfo] << 6) | tp.common[cp_alg];

    for(int op = 0; op < nr_oper; op++) {
	tp.raw[op*7 + 2] = (tp.op[op][op_sr] << 4)
	    | (tp.op[op][op_xof] << 3) | tp.op[op][op_ksr];
	tp.raw[op*7 + 3] = (tp.op[op][op_rr] << 4) | tp.op[op][op_dr];
	tp.raw[op*7 + 4] = (tp.op[op][op_ar] << 4) | tp.op[op][op_sl];
	tp.raw[op*7 + 5] = (tp.op[op][op_tl] << 2) | tp.op[op][op_ksl];
	tp.raw[op*7 + 6] = (tp.op[op][op_dam] << 5) | (tp.op[op][op_eam] << 4)
	    | (tp.op[op][op_dvb] << 1) | tp.op[op][op_evb];
	tp.raw[op*7 + 7] = (tp.op[op][op_multi] << 4) | tp.op[op][op_dt];
	tp.raw[op*7 + 8] = (tp.op[op][op_ws] << 3) | tp.op[op][op_fb];
    }
}

void unpack(void)
{
    tp.common[cp_bo] = tp.raw[0] & 3;
    tp.common[cp_lfo] = tp.raw[1] >> 6;
    tp.common[cp_alg] = tp.raw[1] & 7;
    for(int op = 0; op < nr_oper; op++) {
	tp.op[op][op_sr] = tp.raw[op*7 + 2] >> 4;
	tp.op[op][op_xof] = (tp.raw[op*7 + 2] >> 3) & 1;
	tp.op[op][op_ksr] = tp.raw[op*7 + 2] & 1;

	tp.op[op][op_rr] = tp.raw[op*7 + 3] >> 4;
	tp.op[op][op_dr] = tp.raw[op*7 + 3] & 15;

	tp.op[op][op_ar] = tp.raw[op*7 + 4] >> 4;
	tp.op[op][op_sl] = tp.raw[op*7 + 4] & 15;

	tp.op[op][op_tl] = tp.raw[op*7 + 5] >> 2;
	tp.op[op][op_ksl] = tp.raw[op*7 + 5] & 3;

	tp.op[op][op_dam] = (tp.raw[op*7 + 6] >> 5) & 3;
	tp.op[op][op_eam] = (tp.raw[op*7 + 6] >> 4) & 1;
	tp.op[op][op_dvb] = (tp.raw[op*7 + 6] >> 1) & 3;
	tp.op[op][op_evb] = (tp.raw[op*7 + 6]) & 1;

	tp.op[op][op_multi] = tp.raw[op*7 + 7] >> 4;
	tp.op[op][op_dt] = tp.raw[op*7 + 7] & 7;

	tp.op[op][op_ws] = tp.raw[op*7 + 8] >> 3;
	tp.op[op][op_fb] = tp.raw[op*7 + 8] & 7;
    }
}

int main(int argc, char *argv[])
{
    sd1_open();
    sd1_reset();
    sd1_set_channel(0, 0);
    command_loop();
    return 0;
}
