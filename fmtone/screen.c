#include <stdint.h>
#include <unistd.h> /* for usleep */
#include <ncurses.h>
#include "common.h"

enum screen_coord {
    basey_comm = 0,
    posy_comm = basey_comm + 4,

    posy_algo = basey_comm + 6,
    height_algo = 5,

    posx_ws = 44,
    posy_ws = basey_comm + 3,
    height_ws = 9,

    basey_oper = 13,
    posy_oper = basey_oper + 2,

    basey_raw = posy_oper + nr_oper + 2,
};

/*----------------------------------------------------------------------
 * ボイス共通パラメータ
 */
static const char * const comm_param_frame[] = {
    "YMF825 Tone Editor",
    "",
    "Common      PG",
   /*01234567890123456789*/
    "BO LFO ALG  Oct Len",
};

static const int comm_param_posx[] = {
    1, 5, 9, 14, 18
};

static const uint8_t comm_param_max[] = {
    3, 3, 7, 7, 7
};

/*----------------------------------------------------------------------
 * オペレータごとパラメータ
 */
static const char * const oper_param_frame[] = {
    "     Envelope                       Modulation",
   /*0123456789012345678901234567890123456789012345678901234567890123456789*/
    "     AR DR SL SR RR TL KSR KSL XOF  MULTI DT WS FB EAM DAM EVB DVB",
    "OP1",
    "OP2",
    "OP3",
    "OP4",
    "",
    "Raw values"
};

static int oper_param_posx[] = {
    5, 8, 11, 14, 17, 20, 25, 29, 33,
    39, 43, 45, 49, 53, 57, 61, 65,
};

static uint8_t oper_param_max[] = {
    15, 15, 15, 15, 15, 63, 1, 3, 1,
    15, 7, 31, 7, 1, 3, 1, 3,
};


/*----------------------------------------------------------------------
 * アルゴリズム
 */

static const char * const algo_aa_list[nr_algo][height_algo] = {
    {
	"                                          ",
	"  #####     #####                         ",
	"+-# 1 #-+--># 2 #-->                      ",
	"| ##### |   #####                         ",
	"+-------+                                 ",
    }, {
	"        +---------+-->                    ",
	"  ##### |   ##### |                       ",
	"+-# 1 #-+   # 2 #-+                       ",
	"| ##### |   #####                         ",
	"+-------+                                 ",
    }, {
	"        +---------+---------+---------+-->",
	"  ##### |   ##### |   ##### |   ##### |   ",
	"+-# 1 #-+   # 2 #-+ +-# 3 #-+   # 4 #-+   ",
	"| ##### |   #####   | ##### |   #####     ",
	"+-------+           +-------+             ",
    }, {
	"        +-------------------+             ",
	"  ##### |   #####     ##### |   #####     ",
	"+-# 1 #-+   # 2 #-----# 3 #-+---# 4 #-->  ",
	"| ##### |   #####     #####     #####     ",
	"+-------+                                 ",
    }, {
	"                                          ",
	"  #####     #####     #####     #####     ",
	"+-# 1 #-+---# 2 #-----# 3 #-----# 4 #-->  ",
	"| ##### |   #####     #####     #####     ",
	"+-------+                                 ",
    }, {
	"                  +-------------------+-->",
	"  #####     ##### |   #####     ##### |   ",
	"+-# 1 #-+---# 2 #-+ +-# 3 #-+---# 4 #-+   ",
	"| ##### |   #####   | ##### |   #####     ",
	"+-------+           +-------+             ",
    }, {
	"        +-----------------------------+-->",
	"  ##### |   #####     #####     ##### |   ",
	"+-# 1 #-+   # 2 #-----# 3 #-----# 4 #-+   ",
	"| ##### |   #####     #####     #####     ",
	"+-------+                                 ",
    }, {
	"        +-------------------+---------+-->",
	"  ##### |   #####     ##### |   ##### |   ",
	"+-# 1 #-+   # 2 #-----# 3 #-+   # 4 #-+   ",
	"| ##### |   #####     #####     #####     ",
	"+-------+                                 ",
    }
};


static const char * const ws_aa_list[nr_ws][height_ws] = {
    /* 0 */
    "|    _-~-_                     ",
    "|  -       -                   ",
    "| /         \\                 /",
    "|/           \\               / ",
    "+-------------\\-------------+--",
    "|              \\           /   ",
    "|               \\         /    ",
    "|                -       -     ",
    "|                  ~-_-~       ",
    
    /* 1 */
    "|    _-~-_                     ",
    "|  -       -                   ",
    "| /         \\                 /",
    "|/           \\               / ",
    "+-------------\\-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 2 */
    "|    _-~-_         _-~-_       ",
    "|  -       -     -       -     ",
    "| /         \\   /         \\   /",
    "|/           \\ /           \\ / ",
    "+-------------\\-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 3 */
    "|    _-|           _-|         ",
    "|  -   |         -   |         ",
    "| /    |        /    |        /",
    "|/     |       /     |       / ",
    "+------+------/------+------/--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 4 */
    "|  /\\                          ",
    "| |  |                        |",
    "||    |                      | ",
    "||    |                      | ",
    "+------|------|-------------+--",
    "|       |    |                 ",
    "|       |    |                 ",
    "|        |  |                  ",
    "|         \\/                   ",

    /* 5 */
    "|  /\\     /\\                   ",
    "| |  |   |  |                  ",
    "||    | |    |                 ",
    "||    | |    |                 ",
    "+------+------+-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 6 */
    "|-------------               --",
    "|             |             |  ",
    "|             |             |  ",
    "|             |             |  ",
    "+-------------+-------------+--",
    "|             |             |  ",
    "|             |             |  ",
    "|             |             |  ",
    "|              -------------   ",

    /* 7 */
    "|\\                          |--",
    "|  -\\                       |  ",
    "|    --\\                    |  ",
    "|       ----\\               |  ",
    "+---------------------------+--",
    "|               \\----       |  ",
    "|                    \\--    |  ",
    "|                       \\-  |  ",
    "|                          \\|  ",

    /* 8 */
    "|    -----                     ",
    "|  -       -                   ",
    "| /         \\                 /",
    "|/           \\               / ",
    "+-------------\\-------------+--",
    "|              \\           /   ",
    "|               \\         /    ",
    "|                -       -     ",
    "|                  -----       ",

    /* 9 */
    "|    -----                     ",
    "|  -       -                   ",
    "| /         \\                 /",
    "|/           \\               / ",
    "+-------------\\-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 10 */
    "|    -----         -----       ",
    "|  -       -     -       -     ",
    "| /         \\   /         \\   /",
    "|/           \\ /           \\ / ",
    "+-------------\\-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 11 */
    "|    --|          --|          ",
    "|  -   |        -   |        - ",
    "| /    |       /    |       /  ",
    "|/     |      /     |      /   ",
    "+------+------------+----------",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 12 */
    "|                              ",
    "|  __                          ",
    "| /  \\                        /",
    "||    |                      | ",
    "+------|------|-------------+--",
    "|       |    |                 ",
    "|        \\  /                  ",
    "|         ~~                   ",
    "|                              ",

    /* 13 */
    "|                              ",
    "|  __     __                   ",
    "| /  \\   /  \\                  ",
    "||    | |    |                 ",
    "+------|------|-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 14 */
    "|-------------               --",
    "|             |             |  ",
    "|             |             |  ",
    "|             |             |  ",
    "+-------------+-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 15 */
    "|                              ",
    "|                              ",
    "|         NOT DEFINED          ",
    "|                              ",
    "+-------------+-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 16 */
    "|                              ",
    "|     / \\                      ",
    "|   /     \\                    ",
    "| /         \\                 /",
    "+-------------\\-------------+--",
    "|               \\         /    ",
    "|                 \\     /      ",
    "|                   \\ /        ",
    "|                              ",

    /* 17 */
    "|                              ",
    "|     / \\                      ",
    "|   /     \\                    ",
    "| /         \\                 /",
    "+-------------\\-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 18 */
    "|                              ",
    "|     / \\           / \\        ",
    "|   /     \\       /     \\      ",
    "| /         \\   /         \\   /",
    "+-------------\\-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 19 */
    "|                              ",
    "|     /|            /|         ",
    "|   /  |          /  |         ",
    "| /    |        /    |        /",
    "+-------------+-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 20 */
    "|                              ",
    "|  /\\                          ",
    "| /  \\                        /",
    "|/    \\                      / ",
    "+------\\------/-------------+--",
    "|       \\    /                 ",
    "|        \\  /                  ",
    "|         \\/                   ",
    "|                              ",

    /* 21 */
    "|                              ",
    "|  /\\     /\\                   ",
    "| /  \\   /  \\                 /",
    "|/    \\ /    \\               / ",
    "+------+------+-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 22 */
    "|------        ------        --",
    "|      |      |      |      |  ",
    "|      |      |      |      |  ",
    "|      |      |      |      |  ",
    "+------+------+------+------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 23 */
    "|                              ",
    "|                              ",
    "|         NOT DEFINED          ",
    "|                              ",
    "+-------------+-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 24 */
    "|         __/~|                ",
    "|      __/    |                ",
    "|   __/       |                ",
    "| _/          |               _",
    "+-------------+-------------+--",
    "|             |          /~    ",
    "|             |       /~~      ",
    "|             |    /~~         ",
    "|             |_/~~            ",

    /* 25 */
    "|         __/~|                ",
    "|      __/    |                ",
    "|   __/       |                ",
    "| _/          |               _",
    "+-------------+-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 26 */
    "|         __/~|         __/~|  ",
    "|      __/    |      __/    |  ",
    "|   __/       |   __/       |  ",
    "| _/          | _/          | _",
    "+-------------+-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 27 */
    "|                              ",
    "|      _             _         ",
    "|   __/|          __/|         ",
    "| _/   |        _/   |        _",
    "+------+------+------+------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 28 */
    "|                              ",
    "|      _                       ",
    "|   __/|                       ",
    "| _/   |                      _",
    "+------+------+------+------+--",
    "|      |   /~                  ",
    "|      |/~~                    ",
    "|      ~                       ",
    "|                              ",

    /* 29 */
    "|                              ",
    "|      _      _                ",
    "|   __/|   __/|                ",
    "| _/   | _/   |               _",
    "+------+------+------+------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 30 */
    "|------                      --",
    "|      |                    |  ",
    "|      |                    |  ",
    "|      |                    |  ",
    "+------+------+------+------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",

    /* 31 */
    "|                              ",
    "|                              ",
    "|         NOT DEFINED          ",
    "|                              ",
    "+-------------+-------------+--",
    "|                              ",
    "|                              ",
    "|                              ",
    "|                              ",
};

/*----------------------------------------------------------------------
 */
static void draw_frame(void)
{
    int i;
    for(i = 0; i < sizeof(comm_param_frame) / sizeof(char*); i++) {
	move(basey_comm + i, 0);
	addstr(comm_param_frame[i]);
    }
    for(i = 0; i < sizeof(oper_param_frame) / sizeof(char*); i++) {
	move(basey_oper + i, 0);
	addstr(oper_param_frame[i]);
    }
}

static void draw_params(void)
{
    int i, width, op;

    /* 共通パラメータ */
    for(i = 0; i < nr_comm_params; i++) {
	width = (comm_param_max[i] >= 10) ? 2 : 1;
	move(posy_comm, comm_param_posx[i]);
	printw("%*d", width, tp.common[i]);
    }
    printw(" (%dHz, %dms)      ",  (440*8) >> (7 - tp.common[cp_oct]),
	   2000 >> tp.common[cp_len]);

    /* アルゴリズム */
    move(posy_algo, 0);
    addstr("Algorithm");
    for(i = 0; i < height_algo; i++) {
	move(posy_algo + 1 + i, 0);
	addstr(algo_aa_list[tp.common[cp_alg]][i]);
    }

    /* オペレータごとパラメータ */
    for(op = 0; op < nr_oper; op++) {
	for(i = 0; i < nr_oper_params; i++) {
	    width = (oper_param_max[i] >= 10) ? 2 : 1;
	    move(posy_oper + op, oper_param_posx[i]);
	    printw("%*d", width, tp.op[op][i]);
	}
    }

    /* 波形 */
    if(tp.cury >= 0) {
	move(posy_ws, posx_ws);
	printw("Wave Shape (WS=%d)", tp.op[tp.cury][op_ws]);
	for(i = 0; i < height_ws; i++) {
	    move(posy_ws + 1 + i, posx_ws);
	    addstr(ws_aa_list[tp.op[tp.cury][op_ws]][i]);
	}
    }

    /* レジスタ設定値 */
    pack();
    move(basey_raw, 0);
    for(i = 0; i < param_size; i++) {
	printw("%3d", tp.raw[i]);
	if(i != param_size - 1)
	    printw(",");
    }
}

static void init_screen(void)
{
    initscr();
    erase();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    draw_frame();
}

static void import_tone(void)
{
    move(basey_raw - 1, 0);
    clrtobot();
    addstr("Enter tone parameters:\n");

    char buf[150];
    echo();
    getnstr(buf, sizeof(buf));
    noecho();

    pack();
    unsigned value = 0, dest = 0;
    for(int i = 0; i < sizeof(buf); i++) {
	if(buf[i] == ',' || buf[i] == 0) {
	    tp.raw[dest] = value;
	    value = 0;
	    if(++dest >= param_size)
		break;
	}
	if(buf[i] == 0)
	    break;
	if(buf[i] >= '0' && buf[i] <= '9')
	    value = value * 10 + buf[i] - '0';
    }
    unpack();

    draw_params();
    refresh();
}

/*----------------------------------------------------------------------
 */

void command_loop(void)
{
    init_screen();

    tp.curx = 0;
    tp.cury = -1;
    tp.common[cp_bo] = 1;
    tp.common[cp_oct] = 4;
    tp.common[cp_len] = 2;
    for(int op = 0; op < nr_oper; op++) {
	tp.op[op][op_ar] = 8;
	tp.op[op][op_rr] = 4;
	tp.op[op][op_multi] = 1;
    }

    for(;;) {
	draw_params();

	/* カーソルを移動 */
	if(tp.cury < 0)
	    move(posy_comm, comm_param_posx[tp.curx]);
	else
	    move(posy_oper + tp.cury, oper_param_posx[tp.curx]);
	refresh();

	/* キー入力受付 */
	int ch = getch();
	switch(ch) {
	case 'Q':
	case 'q':
	    endwin();
	    return;
	case 'L' & 0x1f: /* Ctrl-L */
	    draw_frame();
	    break;

	    /* カーソル移動 */
	case KEY_LEFT:
	case 'h':
	case 'a':
	    --tp.curx;
	    break;
	case KEY_RIGHT:
	case 'l':
	case 'd':
	    ++tp.curx;
	    break;
	case KEY_DOWN:
	case 'j':
	    ++tp.cury;
	    break;
	case KEY_UP:
	case 'k':
	    --tp.cury;
	    break;

	    /* 値変更 */
	case '+':
	case '=':
	case 'w':
	    if(tp.cury < 0) {
		if(tp.common[tp.curx] < comm_param_max[tp.curx])
		    ++tp.common[tp.curx];
	    } else {
		if(tp.op[tp.cury][tp.curx] < oper_param_max[tp.curx])
		    ++tp.op[tp.cury][tp.curx];
	    }
	    break;
	case '-':
	case '_':
	case 's':
	    if(tp.cury < 0) {
		if(tp.common[tp.curx] != 0)
		    --tp.common[tp.curx];
	    } else {
		if(tp.op[tp.cury][tp.curx] != 0)
		    --tp.op[tp.cury][tp.curx];
	    }
	    break;

	case 'i':
	    import_tone();
	    break;

	case ' ':
	case '\n':
	case '\r':
	    sd1_set_tones(tp.raw, 1);
	    sd1_note_on(0, tp.common[cp_oct], 601, 0x15);
	    usleep(2000000 >> tp.common[cp_len]);
	    sd1_note_off(0);
	    break;
	}

	/* カーソルの動きを規制 */
	int maxx = (tp.cury < 0) ? nr_comm_params : nr_oper_params;
	if(tp.curx < 0)
	    tp.curx = 0;
	else if(tp.curx >= maxx)
	    tp.curx = maxx - 1;
	if(tp.cury < -1)
	    tp.cury = -1;
	else if(tp.cury >= nr_oper)
	    tp.cury = nr_oper - 1;
    }
}
