/*----------------------------------------------------------------------
 * オペレータ間共通パラメータ
 */
enum comm_param {
    cp_bo, cp_lfo, cp_alg,
    cp_oct, cp_len, /* 擬似パラメータ */
    nr_comm_params
};

/*----------------------------------------------------------------------
 * オペレータごとパラメータ
 */

enum oper_param {
    op_ar, op_dr, op_sl, op_sr, op_rr, op_tl, op_ksl, op_ksr, op_xof,
    op_multi, op_dt, op_ws, op_fb, op_eam, op_dam, op_evb, op_dvb,
    nr_oper_params
};

/*----------------------------------------------------------------------
 */

enum ymf825_consts {
    nr_algo = 8,
    nr_ws = 32,
    nr_oper = 4,
    param_sz_common = 2,
    param_sz_per_oper = 7,
    param_size = param_sz_common + nr_oper * param_sz_per_oper,
};

/*----------------------------------------------------------------------
 */
struct tone_param {
    uint8_t common[nr_comm_params];
    uint8_t op[nr_oper][nr_oper_params];
    uint8_t raw[param_size];
    int curx, cury;
};

extern struct tone_param tp;

/*----------------------------------------------------------------------
 */
extern void command_loop(void);
void pack(void);
void unpack(void);

/*----------------------------------------------------------------------
 */
void sd1_open(void);
void sd1_reset(void);
void sd1_set_tones(const uint8_t *param, unsigned n);
void sd1_set_channel(uint8_t chan, uint8_t tone);
void sd1_note_on(uint8_t chan, uint8_t block, uint16_t fnum, uint8_t vovol);
void sd1_note_off(uint8_t chan);
